namespace CHOOOSEBackendTaskTest;

using CHOOOSEBackendTask;

public class Tests
{
  [SetUp]
  public void Setup() {
  }

  [Test]
  public void TestInvalidWorkdayException() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    Assert.That(() => calendar.SetWorkdayStartAndStop(7, 0, 6, 0), Throws.Exception.TypeOf<InvalidWorkdayException>());
  }

  [Test]
  public void TestInvalidWorkdayTimeException() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    Assert.That(() => calendar.SetWorkdayStartAndStop(8, 0, 26, 0), Throws.Exception.TypeOf<InvalidWorkdayTimeException>());
  }

  [Test]
  public void TestInvalidRecurringDateException() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    Assert.That(() => calendar.SetRecurringHoliday(13, 1), Throws.Exception.TypeOf<InvalidRecurringDateException>());
  }

  [Test]
  public void TestGetWorkdayIncrement1() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    calendar.SetWorkdayStartAndStop(8, 0, 16, 0);
    calendar.SetRecurringHoliday(5, 17);
    calendar.SetHoliday(new DateTime(2004, 5, 27));
    Assert.That(
      calendar.GetWorkdayIncrement(new DateTime(2004, 5, 24, 18, 5, 0), -5.5m),
      Is.EqualTo(new DateTime(2004, 5, 14, 12, 0, 0))
    );
  }

  [Test]
  public void TestGetWorkdayIncrement2() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    calendar.SetWorkdayStartAndStop(8, 0, 16, 0);
    calendar.SetRecurringHoliday(5, 17);
    calendar.SetHoliday(new DateTime(2004, 5, 27));
    Assert.That(
      calendar.GetWorkdayIncrement(new DateTime(2004, 5, 24, 19, 3, 0), 44.723656m),
      Is.EqualTo(new DateTime(2004, 7, 27, 13, 47, 0))
    );
  }

  [Test]
  public void TestGetWorkdayIncrement3() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    calendar.SetWorkdayStartAndStop(8, 0, 16, 0);
    calendar.SetRecurringHoliday(5, 17);
    calendar.SetHoliday(new DateTime(2004, 5, 27));
    Assert.That(
      calendar.GetWorkdayIncrement(new DateTime(2004, 5, 24, 18, 3, 0), -6.7470217m),
      Is.EqualTo(new DateTime(2004, 5, 13, 10, 2, 0))
    );
  }

  [Test]
  public void TestGetWorkdayIncrement4() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    calendar.SetWorkdayStartAndStop(8, 0, 16, 0);
    calendar.SetRecurringHoliday(5, 17);
    calendar.SetHoliday(new DateTime(2004, 5, 27));
    Assert.That(
      calendar.GetWorkdayIncrement(new DateTime(2004, 5, 24, 8, 3, 0), 12.782709m),
      Is.EqualTo(new DateTime(2004, 6, 10, 14, 18, 0))
    );
  }

  [Test]
  public void TestGetWorkdayIncrement5() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    calendar.SetWorkdayStartAndStop(8, 0, 16, 0);
    calendar.SetRecurringHoliday(5, 17);
    calendar.SetHoliday(new DateTime(2004, 5, 27));
    Assert.That(
      calendar.GetWorkdayIncrement(new DateTime(2004, 5, 24, 7, 3, 0), 8.276628m),
      Is.EqualTo(new DateTime(2004, 6, 4, 10, 12, 0))
    );
  }

  [Test]
  public void TestGetWorkdayIncrement6() {
    IWorkdayCalendar calendar = new WorkdayCalendar();
    calendar.SetWorkdayStartAndStop(8, 0, 16, 0);
    calendar.SetRecurringHoliday(5, 17);
    calendar.SetHoliday(new DateTime(2004, 5, 27));
    Assert.That(
      calendar.GetWorkdayIncrement(new DateTime(2004, 5, 24, 7, 3, 0), 0),
      Is.EqualTo(new DateTime(2004, 5, 24, 7, 3, 0))
    );
  }
}