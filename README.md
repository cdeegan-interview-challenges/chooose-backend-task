# CHOOOSE Backend Task

Developed using Dotnet version 7.0.400

## Getting started

This project is split into two parts `CHOOOSEBackendTask` and `CHOOOSEBackendTaskTest`, for source code and unit tests respectively.

### Run the project

Run the project by navigating to the source code using `cd CHOOOSEBackendTask` and running `dotnet run`. Change the variables in the source code to update the output of the program.

### Run the tests

Run the tests by navigating to the source code using `cd CHOOOSEBackendTaskTest` and running `dotnet test`.

## Remarks

### Design decisions
- object-oriented approach
- custom errors to improve error handling experience
- inputting a date with an increment of 0 should return the input date
  - this is the only time the program can return a non-working date
  - **rationale**: incrementing any value by 0, regardless of unit, should result in no change to the value

### Room for improvement
- allow input from console so that source doesn't need to be modified
- increased test coverage (e.g. consider extremely large/small increments)
- better usage of built-in C# DateTime/TimeSpan libs might have made the implementation more straightforward
- there was no need to convert calculations to minutes