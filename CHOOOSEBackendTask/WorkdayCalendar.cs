namespace CHOOOSEBackendTask {
  public interface IWorkdayCalendar {
    void SetHoliday(DateTime date);
    void SetRecurringHoliday(int month, int day);
    void SetWorkdayStartAndStop(int startHours, int startMinutes, int stopHours, int stopMinutes);
    DateTime GetWorkdayIncrement(DateTime startDate, decimal incrementInWorkdays);
  }

  [Serializable]
  public class InvalidWorkdayCalendarException : Exception {
    public InvalidWorkdayCalendarException(string message) : base(message) { }
  }

  public class WorkdayCalendar : IWorkdayCalendar {
    private List<DayOfWeek> nonWorkingDays = new List<DayOfWeek>{DayOfWeek.Saturday, DayOfWeek.Sunday};
    private List<DateTime> holidayDates = new List<DateTime>();
    private List<RecurringDate> recurringHolidayDates = new List<RecurringDate>();
    private Workday? workday;

    public void SetWorkdayStartAndStop(int startHours, int startMinutes, int stopHours, int stopMinutes) {
      workday = new Workday(new WorkdayTime(startHours, startMinutes), new WorkdayTime(stopHours, stopMinutes));
    }

    public void SetHoliday(DateTime date) {
      holidayDates.Add(date);
    }

    public void SetRecurringHoliday(int month, int day) {
      recurringHolidayDates.Add(new RecurringDate(month, day));
    }

    public DateTime GetWorkdayIncrement(DateTime startDate, decimal incrementInWorkdays) {
      if (workday == null) {
        throw new InvalidWorkdayCalendarException("Workday start/end time must be declared before invoking GetWorkdayIncrement");
      }

      // incrementing a date by 0 workdays should just return the input startDate
      if (incrementInWorkdays == 0) {
        return startDate;
      }

      DateTime currentDate = startDate;
      decimal incrementInMinutes = getWorkdayIncrementInMinutes(incrementInWorkdays);
      decimal minsToTargetCounter = Math.Abs(incrementInMinutes);

      if (isDateWorkingDay(currentDate) && workday.isDateTimeWithinWorkday(currentDate)) {
        // the input date falls inside a workday, so calculate the time between it and the start/end of the workday
        DateTime currentWorkingDayStartDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, workday.StartTime.Hours, workday.StartTime.Minutes, 0);
        DateTime currentWorkingDayEndDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, workday.EndTime.Hours, workday.EndTime.Minutes, 0);
        int minsFromStartDateToWorkdayBoundary = incrementInWorkdays > 0
          ? (int) currentWorkingDayEndDate.Subtract(currentDate).TotalMinutes
          : (int) currentDate.Subtract(currentWorkingDayStartDate).TotalMinutes;
        if (minsToTargetCounter <= minsFromStartDateToWorkdayBoundary) {
          // the input increment is small enough that this first date contains the target increment date
          return incrementInWorkdays > 0
            ? currentDate.AddMinutes((int) minsToTargetCounter)
            : currentDate.AddMinutes((int) minsToTargetCounter * -1);
        } else {
          // the target increment is on another date, move on and subtract the workday time remainder
          currentDate = getNextOrderedWorkday(currentDate, incrementInWorkdays);
          minsToTargetCounter -= minsFromStartDateToWorkdayBoundary;
        }
      } else {
        // the input date falls outside a workday, pin currentDate to the closest workday start/end
        if (workday.isBeforeWorkingHours(currentDate)) {
          if (incrementInWorkdays > 0) { currentDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, workday.StartTime.Hours, workday.StartTime.Minutes, 0); }
          else { currentDate = getNextOrderedWorkday(currentDate, incrementInWorkdays); }
        } else if (workday.isAfterWorkingHours(currentDate)) {
          if (incrementInWorkdays > 0) { currentDate = getNextOrderedWorkday(currentDate, incrementInWorkdays); }
          else { currentDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, workday.EndTime.Hours, workday.EndTime.Minutes, 0); }
        }
      }

      while (minsToTargetCounter > 0) {
        if (minsToTargetCounter <= workday.getWorkdayMinutesCount()) {
          // the increment remainder is small enough that this date contains the target increment date
          return incrementInWorkdays > 0
            ? currentDate.AddMinutes((int) minsToTargetCounter)
            : currentDate.AddMinutes((int) minsToTargetCounter * -1);
        } else {
          // the target increment is on another date, move on and subtract the workday time remainder
          currentDate = getNextOrderedWorkday(currentDate, incrementInWorkdays);
          minsToTargetCounter -= workday.getWorkdayMinutesCount();
        }
      }

      // this should not be reached since minsToTargetCounter will eventually reach 0
      // workday.getWorkdayMinutesCount() will always be > 0 since workday.startTime < workday.endTime was enforced
      return startDate;
    }

    private DateTime getNextOrderedWorkday(DateTime date, decimal incrementInWorkdays) {
      DateTime nextOrderedWorkday = getNextOrderedDate(date, incrementInWorkdays);
      while (!isDateWorkingDay(nextOrderedWorkday)) {
        nextOrderedWorkday = getNextOrderedDate(nextOrderedWorkday, incrementInWorkdays);
      }
      return nextOrderedWorkday;
    }

    private DateTime getNextOrderedDate(DateTime date, decimal incrementInWorkdays) {
      if (workday == null) { return date; }
      if (incrementInWorkdays > 0) {
        return new DateTime(date.Year, date.Month, date.Day, workday.StartTime.Hours, workday.StartTime.Minutes, 0).AddDays(1);
      } else {
        return new DateTime(date.Year, date.Month, date.Day, workday.EndTime.Hours, workday.EndTime.Minutes, 0).AddDays(-1);
      }
    }

    private bool isDateWorkingDay(DateTime date) {
      // check the day of week is not listed as a non-working day
      if (nonWorkingDays.Contains(date.DayOfWeek)) { return false; }
      // check the date is not listed as a holiday date, ignoring time
      if (holidayDates.Any((holidayDate) => holidayDate.Date == date.Date)) { return false; }
      // check the date is not listed as a recurring holiday date, ignoring time
      return !recurringHolidayDates.Any((recurringDate) => {
        return recurringDate.isMatchingDateTime(date);
      });
    }

    private decimal getWorkdayIncrementInMinutes(decimal incrementInWorkdays) {
      if (workday == null) { return 0; }
      return workday.getWorkdayMinutesCount() * incrementInWorkdays;
    }
  }
}