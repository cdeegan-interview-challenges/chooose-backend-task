namespace CHOOOSEBackendTask {
  class RecurringDate {
    public int Month {
      get;
      set;
    }
    public int Day {
      get;
      set;
    }
    
    public RecurringDate(int dateMonth, int dateDay) {
      if (dateMonth < 1 || dateMonth > 12) {
        throw new InvalidRecurringDateException(string.Format("{0} is not within an acceptable range", dateMonth));
      }
      if (dateDay < 0 || dateDay > 31) {
        // TODO: this could handle the varying month lengths
        throw new InvalidRecurringDateException(string.Format("{0} is not within an acceptable range", dateDay));
      }
      Month = dateMonth;
      Day = dateDay;
    }

    public bool isMatchingDateTime(DateTime date) {
      return date.Month == Month && date.Day == Day;
    }
  }

  [Serializable]
  public class InvalidRecurringDateException : Exception {
    public InvalidRecurringDateException(): base() { }
    public InvalidRecurringDateException(string message) : base(message) { }
    public InvalidRecurringDateException(string message, Exception innerException) : base(message, innerException) { }
  }
}