namespace CHOOOSEBackendTask {
  class Workday {
    public WorkdayTime StartTime {
      get;
      set;
    }
    public WorkdayTime EndTime {
      get;
      set;
    }
    
    public Workday(WorkdayTime startTime, WorkdayTime endTime) {
      if (startTime == null) {
        throw new InvalidWorkdayException("Workday start time is not set");
      } else if (endTime == null) {
        throw new InvalidWorkdayException("Workday end time is not set");
      } else if (startTime.CompareTo(endTime) >= 0) {
        throw new InvalidWorkdayException("Workday start time must be before workday end time");
      }
      StartTime = startTime;
      EndTime = endTime;
    }

    public int getWorkdayMinutesCount() {
      return EndTime.getTotalWorkdayTimeMinutes() - StartTime.getTotalWorkdayTimeMinutes();
    }

    public bool isDateTimeWithinWorkday(DateTime date) {
      TimeSpan startTimeSpan = new TimeSpan(StartTime.Hours, StartTime.Minutes, 0);
      TimeSpan endTimeSpan = new TimeSpan(EndTime.Hours, EndTime.Minutes, 0);
      return date.TimeOfDay >= startTimeSpan && date.TimeOfDay <= endTimeSpan;
    }

    public bool isBeforeWorkingHours(DateTime date) {
      TimeSpan startTimeSpan = new TimeSpan(StartTime.Hours, StartTime.Minutes, 0);
      return date.TimeOfDay < startTimeSpan;
    }

    public bool isAfterWorkingHours(DateTime date) {
      TimeSpan endTimeSpan = new TimeSpan(EndTime.Hours, EndTime.Minutes, 0);
      return date.TimeOfDay > endTimeSpan;
    }
  }

  [Serializable]
  public class InvalidWorkdayException : Exception {
    public InvalidWorkdayException(): base() { }
    public InvalidWorkdayException(string message) : base(message) { }
    public InvalidWorkdayException(string message, Exception innerException) : base(message, innerException) { }
  }
}