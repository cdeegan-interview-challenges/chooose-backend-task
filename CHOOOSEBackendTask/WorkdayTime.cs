namespace CHOOOSEBackendTask {
  class WorkdayTime : IComparable<WorkdayTime> {
    public int Hours {
      get;
      set;
    }
    public int Minutes {
      get;
      set;
    }
    
    public WorkdayTime(int workdayHours, int workdayMinutes) {
      if (workdayHours < 0 || workdayHours > 23) {
        throw new InvalidWorkdayTimeException(string.Format("{0} is not within an acceptable range", workdayHours));
      }
      if (workdayMinutes < 0 || workdayMinutes > 59) {
        throw new InvalidWorkdayTimeException(string.Format("{0} is not within an acceptable range", workdayMinutes));
      }
      Hours = workdayHours;
      Minutes = workdayMinutes;
    }

    public int getTotalWorkdayTimeMinutes() {
      return (Hours * 60) + Minutes;
    }

    public int CompareTo(WorkdayTime? otherTime) {
      if (otherTime == null) { return 1; }
      int thisSum = getTotalWorkdayTimeMinutes();
      int otherSum = otherTime.getTotalWorkdayTimeMinutes();
      return thisSum.CompareTo(otherSum);
    }
  }

  [Serializable]
  public class InvalidWorkdayTimeException : Exception {
    public InvalidWorkdayTimeException(): base() { }
    public InvalidWorkdayTimeException(string message) : base(message) { }
    public InvalidWorkdayTimeException(string message, Exception innerException) : base(message, innerException) { }
  }
}