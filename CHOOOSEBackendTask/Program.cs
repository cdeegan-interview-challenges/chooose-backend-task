﻿using CHOOOSEBackendTask;

try {
  IWorkdayCalendar calendar = new WorkdayCalendar();
  calendar.SetWorkdayStartAndStop(8, 0, 16, 0);
  calendar.SetRecurringHoliday(5, 17);
  calendar.SetHoliday(new DateTime(2004, 5, 27));
  string format = "dd-MM-yyyy HH:mm";
  var start = new DateTime(2004, 5, 24, 18, 5, 0);
  decimal increment = -5.5m;
  var incrementedDate = calendar.GetWorkdayIncrement(start, increment);
  Console.WriteLine(
  start.ToString(format) +
  " with an addition of " +
  increment +
  " work days is " +
  incrementedDate.ToString(format));
} catch (InvalidWorkdayException iwe) {
  // TODO: handle error
  Console.WriteLine("There was a problem with the specified Workday: " + iwe.Message);
} catch (InvalidWorkdayTimeException iwte) {
  Console.WriteLine("There was a problem with the specified WorkdayTime: " + iwte.Message);
} catch (InvalidRecurringDateException irde) {
  Console.WriteLine("There was a problem with the specified RecurringDate: " + irde.Message);
} catch (Exception e) {
  Console.WriteLine("There was an unhandled exception: " + e.Message);
}
